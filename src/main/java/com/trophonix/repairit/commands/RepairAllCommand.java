package com.trophonix.repairit.commands;

import com.trophonix.repairit.RepairItPlugin;
import com.trophonix.repairit.util.EconomyHandler;
import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class RepairAllCommand implements CommandExecutor {

  private static boolean damageable1_9;
  static {
    try {
      Class.forName("org.bukkit.inventory.meta.Damageable");
      damageable1_9 = true;
    } catch (ClassNotFoundException ignored) {
    }
  }

  private final RepairItPlugin pl;

  private List<Player> largeRepairs = new ArrayList<>();

  @Override public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage(ChatColor.RED + "Gotta be a player to do that, partner!");
      return true;
    }

    Player player = (Player) sender;
    List<ItemStack> repairable = new ArrayList<>();
    int totalDamage = 0;

    for (ItemStack item : player.getInventory()) {
      if (item == null) continue;
      if (damageable1_9) {
        ItemMeta meta = item.getItemMeta();
        if (meta instanceof Damageable) {
          Damageable damageable = (Damageable) meta;
          if (!damageable.hasDamage()) continue;
          repairable.add(item);
          totalDamage += damageable.getDamage();
        }
      } else if (item.getDurability() > 0) {
        repairable.add(item);
        totalDamage += item.getDurability();
      }
    }

    if (repairable.isEmpty()) {
      pl.getConf().getFailureNoRepairableItems().send(player);
      return true;
    }

    if (pl.getEconomy() != null) {
      double originalPrice = totalDamage * pl.getConf().getPricePerDamagePoint();
      double price;
      if (player.hasPermission("RepairIt.free")) price = 0;
      else if (player.hasPermission("RepairIt.reduced")) price = originalPrice * pl.getConf().getReductionFactor();
      else price = originalPrice;
      if (pl.getConf().isRoundPrice()) {
        price = Math.round(price);
      }

      double balance = pl.getEconomy().getBalance(player);
      if (price > balance) {
        pl.getConf().getFailureInsufficientFunds().send(player, "{damage}", Integer.toString(totalDamage),
            "{balance}", EconomyHandler.moneyFormat.format(balance), "{price}", EconomyHandler.moneyFormat.format(price));
        return true;
      }

      pl.getEconomy().remove(player, price);

      String[] placeholders = new String[]{"{damage}", Integer.toString(totalDamage),
          "{balance}", EconomyHandler.moneyFormat.format(balance), "{price}", EconomyHandler.moneyFormat.format(price),
          "{originalPrice}", EconomyHandler.moneyFormat.format(originalPrice), "{items}", Integer.toString(repairable.size())};

      if (pl.getConf().isConfirmLargeRepair() && totalDamage > pl.getConf().getLargeRepairMinimum()) {
        if (!largeRepairs.remove(player)) {
          pl.getConf().getConfirmLargeRepairAllMessage().send(player, placeholders);
          largeRepairs.add(player);
          Bukkit.getScheduler().runTaskLater(pl, () -> largeRepairs.remove(player), 60 * 20);
          return true;
        }
      }

      pl.getConf().getRepairAllSuccess().send(player, placeholders);
      if (player.hasPermission("RepairIt.free")) pl.getConf().getFree().send(player, placeholders);
      else if (player.hasPermission("RepairIt.reduced")) pl.getConf().getReduced().send(player, placeholders);
      else pl.getConf().getFull().send(player, placeholders);
    }
    repairable.forEach(item -> {
      ItemMeta meta = item.getItemMeta();
      if (meta != null) {
        if (damageable1_9) {
          ((Damageable) meta).setDamage(0);
          item.setItemMeta(meta);
        } else {

          item.setDurability((short)0);
        }
      }
    });
    return true;
  }
}
