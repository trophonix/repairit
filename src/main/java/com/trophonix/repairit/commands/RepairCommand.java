package com.trophonix.repairit.commands;

import com.trophonix.repairit.RepairItPlugin;
import com.trophonix.repairit.util.EconomyHandler;
import com.trophonix.repairit.util.Utils;
import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class RepairCommand implements CommandExecutor {

  private static boolean damageable1_9;
  static {
    try {
      Class.forName("org.bukkit.inventory.meta.Damageable");
      damageable1_9 = true;
    } catch (ClassNotFoundException ignored) {
    }
  }

  private final RepairItPlugin pl;

  private List<Player> largeRepairs = new ArrayList<>();

  @Override public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage(ChatColor.RED + "Gotta be a player to do that, partner!");
      return true;
    }

    Player player = (Player) sender;
    ItemStack heldItem = Utils.getHeldItem(player);
    ItemMeta meta = heldItem.getItemMeta();
    int damage;
    if (damageable1_9) {
      if (!(meta instanceof Damageable)) {
        pl.getConf().getInvalidItem().send(player, "{itemType}", Utils.getPrettyName(heldItem.getType()));
        return true;
      }

      Damageable damageable = (Damageable) meta;
      if (!damageable.hasDamage()) {
        pl.getConf().getFailureAlreadyRepaired().send(player, "{damage}", Integer.toString(damageable.getDamage()));
        return true;
      }
      damage = damageable.getDamage();
    } else {
      damage = heldItem.getDurability();
    }

    int toRepair;
    if (command.getName().equals("repair") && args.length > 0) {
      try {
        toRepair = Integer.parseInt(args[0]);
        if (toRepair > damage) toRepair = damage;
      } catch (NumberFormatException ignored) {
        pl.getConf().getInvalidNumber().send(player, "{input}", args[0]);
        return true;
      }
    } else {
      toRepair = damage;
    }

    String itemType = Utils.getPrettyName(heldItem.getType());
    String itemName = meta.hasDisplayName() ? meta.getDisplayName() : itemType;
    if (pl.getEconomy() != null) {
      double originalPrice = pl.getConf().getPricePerDamagePoint() * toRepair, price;
      if (player.hasPermission("RepairIt.free")) price = 0;
      else if (player.hasPermission("RepairIt.reduced")) price = originalPrice * pl.getConf().getReductionFactor();
      else price = originalPrice;

      if (pl.getConf().isRoundPrice()) {
        price = Math.round(price);
      }

      if (pl.getEconomy().getBalance(player) < price) {
        pl.getConf().getFailureInsufficientFunds().send(player, "{input}", args.length > 0 ? args[0] : "",
            "{damage}", Integer.toString(toRepair), "{price}", EconomyHandler.moneyFormat.format(price),
            "{balance}", pl.getEconomy().getBalanceString(player), "{item}", itemName,
            "{itemType}", itemType);
        return true;
      }

      String[] placeholders = new String[]{"{input}", args.length > 0 ? args[0] : "",
          "{damage}", Integer.toString(toRepair), "{price}", EconomyHandler.moneyFormat.format(price),
          "{balance}", pl.getEconomy().getBalanceString(player), "{item}", itemName,
          "{itemType}", itemType, "{originalPrice}", EconomyHandler.moneyFormat.format(originalPrice),
          "{reduced}", EconomyHandler.moneyFormat.format(price)};

      if (pl.getConf().isConfirmLargeRepair() && toRepair >= pl.getConf().getLargeRepairMinimum()) {
        if (!largeRepairs.remove(player)) {
          pl.getConf().getConfirmLargeRepairMessage().send(player, placeholders);
          largeRepairs.add(player);
          Bukkit.getScheduler().runTaskLater(pl, () -> largeRepairs.remove(player), 60 * 20);
          return true;
        }
      }

      pl.getEconomy().remove(player, price);
      if (damageable1_9) ((Damageable)meta).setDamage(damage - toRepair);
      else heldItem.setDurability((short)(damage - toRepair));
      heldItem.setItemMeta(meta);

      pl.getConf().getRepairSuccess().send(player, "{input}", args.length > 0 ? args[0] : "",
          "{damage}", Integer.toString(toRepair), "{price}", EconomyHandler.moneyFormat.format(price),
          "{balance}", pl.getEconomy().getBalanceString(player), "{item}", itemName,
          "{itemType}", itemType);

      if (player.hasPermission("RepairIt.free"))
        pl.getConf().getFree().send(player, placeholders);
      else if (player.hasPermission("RepairIt.reduced"))
        pl.getConf().getReduced().send(player, placeholders);
      else pl.getConf().getFull().send(player, placeholders);
    } else {
      if (damageable1_9) ((Damageable)meta).setDamage(damage - toRepair);
      else heldItem.setDurability((short)(damage - toRepair));
      pl.getConf().getRepairSuccess().send(sender, "{input}", args.length > 0 ? args[0] : "",
          "{damage}", Integer.toString(toRepair), "{balance}", "0", "{item}", itemName,
          "{itemType}", itemType, "{price}", "0");
      heldItem.setItemMeta(meta);
    }
    return true;
  }
}
