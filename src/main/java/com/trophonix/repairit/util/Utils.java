package com.trophonix.repairit.util;

import org.apache.commons.lang.WordUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Utils {

  public static String replaceIgnoreCase(String source, String target, String replacement) {
    StringBuilder sbSource = new StringBuilder(source);
    StringBuilder sbSourceLower = new StringBuilder(source.toLowerCase());
    String searchString = target.toLowerCase();

    int idx = 0;
    while((idx = sbSourceLower.indexOf(searchString, idx)) != -1) {
      sbSource.replace(idx, idx + searchString.length(), replacement);
      sbSourceLower.replace(idx, idx + searchString.length(), replacement);
      idx+= replacement.length();
    }
    sbSourceLower.setLength(0);
    sbSourceLower.trimToSize();
    return sbSource.toString();
  }

  public static String getPrettyName(Material material) {
    return WordUtils.capitalizeFully(material.name().toLowerCase().replace("_", " "));
  }

  private static boolean heldItem1_9;

  public static ItemStack getHeldItem(Player player) {
    if (heldItem1_9) return player.getItemInHand();
    try {
      return HeldItem1_9.getHeldItem(player);
    } catch (NoSuchMethodError ignored) {
      heldItem1_9 = true;
      return player.getItemInHand();
    }
  }

}
